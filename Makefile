.ONESHELL:

define usage
column -t -s '	' << EOF
help	this help menu
resources	download and prepare resources used in this study
analysis	run all analyses
report	build manuscript
submission	create submission
EOF
endef

include config.mk

help:
	@$(usage)

.PHONY: analysis
analysis:
	$(MAKE) -C analysis

.PHONY: resources
resources:
	$(MAKE) -C resources

.PHONY: report
report: analysis
	$(MAKE) -C doc

DESTDIR ?= $(CURDIR)/submission/$(VERSION)

.PHONY: submission
submission: code-dist
	mkdir -p $(DESTDIR)
	$(MAKE) -C doc
	cp doc/main.submission.tex $(DESTDIR)/main.tex
	cp doc/references.bib $(DESTDIR)/
	cp doc/main.pdf $(DESTDIR)/
	cp doc/Supplemental.pdf $(DESTDIR)/S1_supplementary-note.pdf
	mkdir -p $(DESTDIR)/assets
	cp doc/assets/figure?-*.pdf $(DESTDIR)/assets/
	tar -C submission -c $(VERSION) -O \
	| gzip > $$(basename $(topsrcdir) .pub)-$(VERSION).tar.gz

.PHONY: code-dist
code-dist: $(DESTDIR)/S1_analysis-code.tar

$(DESTDIR)/S1_analysis-code.tar:
	git archive --prefix=$(@F:.tar=)-$(VERSION)/ HEAD \
	README.md \
	Makefile \
	config.mk \
	analysis \
	resources \
	include \
	-o $@

# Generic rules

# [!] This will fail if you have an instance of Libreoffice open.
%.xlsx: %.tsv
	libreoffice \
	--infilter="Text - txt - csv (StarCalc):9,,0,1," \
	--convert-to xlsx $<

%: %.gz
	gunzip -c $< > $@
